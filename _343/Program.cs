﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using _343.Helpers;

namespace _343
{
    class Program
    {
        public static string SERVER = "irc.rizon.net";
        public static int PORT = 6667;
        public static string OWNER = "Frankity";
        public static string USER = "Axey Frankity's Bot";
        private static string NICK = "Axey";
        private static string CHANNEL = "#Axey";

        public static void Main(string[] args)
        {

            BlackList.ReadBList();

#if !DEGUB
            Log.Initialize("logs/" + DateTime.Now.ToString().Replace("/", "_").Replace(":", "_") + "_Axey.log", LogLevel.Data | LogLevel.Info | LogLevel.Error, true);
#else
            Log.Initialize("logs/Axey.log", LogLevel.All, true);
#endif

            Dictionary<string, APlugin.APlugin> _Plugins;

            // ---------------------- Plugins -------------------- //
/*
            _Plugins = new Dictionary<string, APlugin.APlugin>();
            ICollection<APlugin.APlugin> plugins = GenLoadPlugin<APlugin.APlugin>.LoladPlugins("plugins");
            foreach (var item in plugins)
            {
                _Plugins.Add(item.Name, item);
                Log.Info("Plugin " + item.Name + " Loaded");
                item.Run();

            }
            */
            // ---------------------- Plugins ------------------- //

            Log.Info("Axey starting...");

            string buf;
            System.Net.Sockets.TcpClient sock = new System.Net.Sockets.TcpClient();
            TextReader input;
            TextWriter output;

            sock.Connect(SERVER, PORT);
            if (sock.Connected)
            {
                Log.Info("Connected..");
            }
            else
            {
                Log.Warn("Failed to connect!");
                return;
            }

            input = new System.IO.StreamReader(sock.GetStream());
            output = new System.IO.StreamWriter(sock.GetStream());

            output.Write(
               "USER " + NICK + " 0 * :" + OWNER + "\r\n" +
               "NICK " + NICK + "\r\n"
            );
            output.Flush();

            for (buf = input.ReadLine(); ; buf = input.ReadLine())
            {
                Log.Info(buf);
                if (buf.StartsWith("PING ")) { output.Write(buf.Replace("PING", "PONG") + "\r\n"); output.Flush(); }
                if (buf[0] != ':') continue;

                string[] omg = buf.Split(' ');

                if (omg[2] != "")
                {
                    if (buf.Contains(omg[2] + " :!cmds"))
                    {
                        output.Write("PRIVMSG " + omg[2] + " :!version -> for get bot Version." + "\r\n"); output.Flush();
                        output.Write("PRIVMSG " + omg[2] + " :!axey -> to get some info about Axey." + "\r\n"); output.Flush();
                        output.Write("PRIVMSG " + omg[2] + " :!time <- not yet implemented." + "\r\n"); output.Flush();
                        output.Write("PRIVMSG " + omg[2] + " :!kick -> use !kick + nick to kick him, this command require operator permissions." + "\r\n"); output.Flush();
                        output.Write("PRIVMSG " + omg[2] + " :!google -> use ! google + term to get a search query." + "\r\n"); output.Flush();
                        output.Write("PRIVMSG " + omg[2] + " :!join -> use !join + #chan to join the bot to the specified channel." + "\r\n"); output.Flush();
                        output.Write("PRIVMSG " + omg[2] + " :!ifly -> use !ifly + some question and wait to see if you feeling lucky." + "\r\n"); output.Flush();
                        output.Write("PRIVMSG " + omg[2] + " :!cmds -> this command display this help." + "\r\n"); output.Flush();
                        output.Write("PRIVMSG " + omg[2] + " :!blacklist -> this command show the channels in the blacklist." + "\r\n"); output.Flush();
                        output.Write("PRIVMSG " + omg[2] + " :!say -> this command sends messages like a normal IRC user." + "\r\n"); output.Flush();
                    }
                    else if (buf.Contains(omg[2] + " :!version"))
                    {
                        output.Write("PRIVMSG " + omg[2] + " :My version is 0.024." + "\r\n"); output.Flush();
                    }
                    else if (buf.Contains(omg[2] + " :!axey"))
                    {
                        var rs = omg[0].Substring(1, omg[0].LastIndexOf('!') - 1);
                        output.Write("PRIVMSG " + rs + " :Hi i'm Axey. \r\na Frankity bot, i'm wroted in c#.\r\ni'm not modules in use right now." + "\r\n"); output.Flush();
                        output.Write("PRIVMSG " + rs + " :A Frankity bot." + "\r\n"); output.Flush();
                        output.Write("PRIVMSG " + rs + " :I'm wroted in c#.\r\ni'm not modules in use right now." + "\r\n"); output.Flush();
                        output.Write("PRIVMSG " + rs + " :No modules in use right now." + "\r\n"); output.Flush();
                        output.Write("PRIVMSG " + rs + " :My source code is here http://https://gitlab.com/Frankity/Axey/tree/master." + "\r\n"); output.Flush();
                    }
                    else if (buf.Contains(omg[2] + " :!google"))
                    {
                        try
                        {
                            output.Write("PRIVMSG " + omg[2] + " :https://www.google.com/search?q=" + omg[4] + "\r\n"); output.Flush();
                        }
                        catch (Exception)
                        {
                            output.Write("PRIVMSG " + omg[2] + " :you must specify some text to find it." + "\r\n"); output.Flush();
                        }
                    }
                    else if (buf.Contains(omg[2] + " :!leave"))
                    {
                        if (buf.Contains(OWNER))
                        {
                            output.Write("PART " + omg[2] + "\r\n"); output.Flush();
                        }
                        else
                        {
                            var rs = omg[0].Substring(1, omg[0].LastIndexOf('!') - 1);
                            output.Write("PRIVMSG " + rs + " :i cant execute that command, you're not my owner... Fagot" + "\r\n"); output.Flush();
                        }
                    }
                    else if (buf.Contains(omg[2] + " :!kick"))
                    {
                        if (buf.Contains(OWNER))
                        {
                            if (omg.Length == 5) 
                            {
                                output.Write("PRIVMSG " + omg[2] + " :can you specify some reason for kick him?" + "\r\n"); output.Flush();
                            } 
                            else 
                            {
                                output.Write("KICK " + omg[2] + " " + omg[4].ToString() + "\r\n"); output.Flush();
                            }
                        }
                        else
                        {

                            output.Write("PRIVMSG " + omg[4] + " :i cant execute that command, you're not my owner... Fagot" + "\r\n"); output.Flush();
                        }
                    }
                    else if (buf.Contains(omg[2] + " :!join"))
                    {
                        if (omg[2] == omg[4])
                        {
                            output.Write("PRIVMSG " + omg[2] + " :dude pls, don't act like a 12..." + "\r\n"); output.Flush();
                        }
                        else
                        {
                            if (BlackList.b.Contains(omg[4].ToString()))
                            {
                                Console.WriteLine("i can't join to that " +  omg[4] + " channel."  );
                            }
                            else
                            {
                                output.Write("JOIN " + omg[4] + "\r\n"); output.Flush();
                            }
                        }
                    }
                    else if (buf.Contains(omg[2] + " :!badd"))
                    {
                        BlackList.ReadBList();
                        if (buf.Contains(OWNER))
                        {
                            if (BlackList.b.Contains(omg[4]))
                            {
                                output.Write("PRIVMSG " + omg[2] + " :this channel is already in the Blacklist." + "\r\n"); output.Flush();
                            }
                            else
                            {
                                BlackList.WriteBlist(omg[4]);
                                output.Write("PRIVMSG " + omg[2] + " :" + omg[4] + " added to the Blacklist." + "\r\n"); output.Flush();
                            }
                        }
                    }
                    else if (buf.Contains(omg[2] + " :!brem"))
                    {
                        BlackList.ReadBList();
                        if (buf.Contains(OWNER))
                        {
                            if (BlackList.b.Contains(omg[4]))
                            {
                                string[] list = BlackList.b.ToArray();
                                var index = Array.FindIndex(list, row => row.Contains(omg[4]));
                                BlackList.RemoveFBlist(index);
                                output.Write("PRIVMSG " + omg[2] + " :" + omg[4] + " has been removed from Blacklist." + "\r\n"); output.Flush();
                            }
                            else
                            {
                                output.Write("PRIVMSG " + omg[2] + " :" + omg[4] + " isn't the Blacklist." + "\r\n"); output.Flush();
                            }
                        }
                    }
                    else if (buf.Contains(omg[2] + " :!say"))
                    {
                        output.Write("CHANSERV REGISTER #Axey America12 SafeHouse"); output.Flush();
                    }

                    else if (buf.Contains("INVITE " + NICK))
                    {
                        output.Write("JOIN " + omg[3] + "\r\n"); output.Flush();
                    }
                    else if (buf.Contains(omg[2] + " :!ifly"))
                    {
                        string[] lucky = new string[] {"It is certain", 
                            "It is decidedly so", 
                            "Without a doubt", 
                            "Yes - definitely",
                            "You may rely on it", 
                            "As I see it, yes",
                            "Most likely", 
                            "Outlook good",
                            "Signs point to yes", 
                            "Yes", 
                            "Reply hazy, try again",
                            "Ask again later", 
                            "Better not tell you now", 
                            "Cannot predict now", 
                            "Concentrate and ask again", 
                            "Don\"t count on it", 
                            "My reply is no", 
                            "My sources say no", 
                            "Outlook not so good", 
                            "Very doubtful"};
                        Random rng = new Random();
                        
                        Random rnd = new Random();
                        int ass = rnd.Next(0, 20);

                        if (1 + 1 == 2)
                        {
                            output.Write("PRIVMSG " + omg[2] + " :" + lucky[ass] + "\r\n"); output.Flush();
                        }
                    }
                    else if (buf.Contains(omg[2] + " :!blacklist"))
                    {
                        foreach (var item in BlackList.b)
                        {
                            output.Write("PRIVMSG " + omg[2] + " :" + item + "\r\n"); output.Flush();    
                        }
                    }
                }

                if (buf.Split(' ')[1] == "001")
                {
                    if (CHANNEL != "")
                    {
                        output.Write("JOIN " + CHANNEL + " " + "\r\n");
                    }
                    output.Write("NOTICE " + OWNER + " :" + NICK + " is now ONLINE." + "\r\n");
                    output.Write("WHOIS " + NICK + "\r\n"); 
                    output.Write("IDENTIFY XXXXXXXXXX \r\n");
                    output.Flush();
                }
            }
        }
    }
}
