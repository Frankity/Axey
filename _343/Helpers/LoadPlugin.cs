﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.Collections;
using APlugin;

namespace _343.Helpers
{
    public static class PluginHelper
    {
        public static ICollection<APlugin.APlugin> LoadPlugins(string path)
        {
            string[] dllFileNames = null;
            if (Directory.Exists(path))
            {
                dllFileNames = Directory.GetFiles(path, "*.dll");
                ICollection<Assembly> assemblies = new List<Assembly>(dllFileNames.Length);
                foreach (string dllFile in dllFileNames)
                {
                    AssemblyName aN = AssemblyName.GetAssemblyName(dllFile);
                    Assembly assembly = Assembly.Load(aN);
                    assemblies.Add(assembly);
                }

                Type pluginType = typeof(APlugin.APlugin);
                ICollection<Type> pluginTypes = new List<Type>();
                foreach (Assembly assembly in assemblies)
                {
                    if (assembly != null)
	                {
		                Type[] types = assembly.GetTypes();
                        foreach (Type type in types)
	                    {
                            if (type.IsInterface || type.IsAbstract)
                                continue;
                            else
                                if (type.GetInterface(pluginType.FullName) != null)
                                    pluginTypes.Add(type);
	                    }       
	                }
                }
                ICollection<APlugin.APlugin> plugins = new List<APlugin.APlugin>(pluginTypes.Count);
                foreach (Type type in pluginTypes)
                {
                    APlugin.APlugin plugin = (APlugin.APlugin)Activator.CreateInstance(type);
                    plugins.Add(plugin);
                }
                return plugins;
            }
            return null;
       }
    }
}
