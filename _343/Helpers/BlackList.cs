﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace _343.Helpers
{
    public class BlackList
    {
        public static List<string> b = new List<string>();
        public static string Blist1 = AppDomain.CurrentDomain.BaseDirectory + @"\files\blacklist.txt";
        public static void ReadBList()
        {
            try
            {
                b = File.ReadAllLines(Blist1).ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);   
            }
        }

        public static void WriteBlist(string channel)
        {
            /// <summary>
            /// <para>This function write the specified channel in the blacklist. </para>
            /// </summary> 
            try
            {
                using (StreamWriter sw = File.AppendText(Blist1))
                {
                    sw.WriteLine(channel + "\n");
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }

        public static void RemoveFBlist(int channel)
        {
            try
            {
                List<string> linelist = File.ReadAllLines(Blist1).ToList();
                linelist.RemoveAt(channel);
                File.WriteAllLines(Blist1,linelist.ToArray());
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }
    }
}
