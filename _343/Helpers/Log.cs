﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Globalization;

namespace _343.Helpers
{
    public enum LogLevel
    {
        None = 0,
        Debug = 1,
        Info = 2,
        Warning = 4,
        Error = 5,
        Data = 16,
        All = 31
    }

    public static class Log
    {
        private static string _filename;
        private static LogLevel _logLevel;
        private static StreamWriter _logWriter;

        public static void Initialize(string filename, LogLevel logLevel, bool clear)
        {
            _filename = filename;
            _logLevel = logLevel;
            _logWriter = new StreamWriter(filename, !clear);
        }

        private static void Write(String message, LogLevel level)
        {
            StackTrace trace = new StackTrace();
            StackFrame frame = null;

            frame = trace.GetFrame(2);
            string caller = "";

            if (frame != null && frame.GetMethod().DeclaringType != null)
            {
                caller = frame.GetMethod().DeclaringType.Name + ": ";
            }

            switch (level)
            {
                case LogLevel.Debug:
                    message = "DEBUG: " + message;
                    break;
                case LogLevel.Info:
                    message = "INFO: " + message;
                    break;
                case LogLevel.Warning:
                    message = "WARNING: " + message;
                    break;
                case LogLevel.Error:
                    message = "EEROR: " + message;
                    break;
            }

            String text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture) + " - " + caller + message;

            Console.WriteLine(text);
            if (!MayWriteType(level))
            {
                return;
            }

            _logWriter.WriteLine(text);
            _logWriter.Flush();
        }

        private static bool MayWriteType(LogLevel type)
        {
            return ((_logLevel & type) == type);
        }

        public static void Data(String message)
        {
            Write(message, LogLevel.Data);
        }

        public static void Error(String message)
        {
            Write(message, LogLevel.Error);
        }

        public static void Warn(String message)
        {
            Write(message, LogLevel.Warning);
        }

        public static void Info(String message)
        {
            Write(message, LogLevel.Info);
        }

        public static void Debug(String message)
        {
            if (!MayWriteType(LogLevel.Debug))
            {
                return;
            }

            Write(message, LogLevel.Debug);
        }
    }

}
