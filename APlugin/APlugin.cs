﻿namespace APlugin
{
    public interface APlugin
    {
        string Name { get; }
        void Run();
        
    }
}
